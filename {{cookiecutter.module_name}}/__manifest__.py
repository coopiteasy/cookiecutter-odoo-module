# SPDX-FileCopyrightText: {% now 'local', '%Y' %} {{ cookiecutter.author }}
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{
    "name": "{{ cookiecutter.module_full_name }}",
    "summary": "{{ cookiecutter.summary }}",
{%- if '9.0' in cookiecutter.odoo_version %}
    "description": "",
{%- endif %}
    "version": "{{ cookiecutter.odoo_version }}.1.0.0",
    "category": "{{ cookiecutter.category }}",
    "website": "https://github.com/{{ cookiecutter.github_owner }}/{{ cookiecutter.repository_slug }}",
    "author": "{{ cookiecutter.author }}{% if cookiecutter.is_oca_module %}, Odoo Community Association (OCA){% endif %}",
    "maintainers": ["{{ cookiecutter.github_user_maintainer }}"],
    "license": "AGPL-3",
{%- if cookiecutter.is_application %}
    "application": True,
{%- endif %}
    "depends": [],
    # Please remove empty values below.
    "data": [],
    "demo": [],
}
